# How to Build a Cloud-Native Marketing Analytics Platform with Matillion ETL for Amazon Redshift

## **Overview**
1. Creating a Matillion Stack via Cloud Formation
2. Matillion Exercises

## **Creating a Matillion Stack via Cloud Formation**
**Prerequisites:**
1. Subscribe to [Matillion ETL for Amazon Redshift](https://aws.amazon.com/marketplace/pp/B010ED5YF8?qid=1532546478798&sr=0-1&ref_=srh_res_product_title) on AWS Marketplace
2. [Create a Key Pair](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair)
    - Save the downloaded .pem file in a safe place
3. [Create an S3 Bucket](https://docs.aws.amazon.com/AmazonS3/latest/gsg/CreatingABucket.html)
4. Download this template: [poc_new.json](https://gitlab.com/david.lipowitz/matillion-workshop/blob/master/poc_new.json)

**Instructions: [Launching Matillion ETL Using Templates](https://redshift-support.matillion.com/s/article/2858081)**
1. Create a new stack through the CloudFormation Console
    - Specify template
        - Upload poc_new.json template file
    - Specify stack details -- must set items below, otherwise user defaults
        - Stack name -- The name of your CloudFormation stack
        - Keypair name -- From prerequisites above, the selected key pair will be added to the set of keys authorized for this instance
        - AZ #1 -- Select an Availability Zone from the current region
        - Inbound IPv4 CIDR -- Inbound IPv4 CIDR Range for Matillion Instance e.g. 0.0.0.0/0
        - Master password -- Redshift Password. Must contain one upper and one lower case letter and one digit but not quotes, slashes, @ or spaces.
    - Configure stack options
        - None required
    - Create stack
        - Check box to acknowledge that AWS CloudFormation might create IAM resources.
2. 5 to 10 minutes to spin up
3. Optionally allow Redshift queries from your laptop
    - Open Redshift cluster to public
        - Modify Cluster
        - Set "Publicly accessible" to Yes
    - Allow traffic through cluster's Security Group
    
## **Matillion Exercises**
**Orchestration, Part 1**
1. [Create a project](https://redshift-support.matillion.com/s/article/1991963)
    - Creates a space to create your jobs
    - Stores AWS and Redshift credentials for use by Matillion
2. [Alter Session WLM Slots](https://documentation.matillion.com/docs/2930333)
    - Set value of Slot Count to `4`
3. [S3 Load Generator](https://redshift-support.matillion.com/s/article/2973428)
    - Publicly available Google Analytics data, use `s3://mtln-workshop/GA360/GA360_structured.csv.gz`
    - Change the table name from GA360_structured.csv to `GA_20170721`
    - Run job to create table and load data from S3
4. [Create Transformation Job](https://documentation.matillion.com/docs/2037606)
    - Right-click the project in the Project Explorer and select `Create Transformation Job`
    - Drag the transformation job onto the workspace as the next step after the S3 Load Component
    - Double-click the transformation job to begin transforming the data which was just loaded

**Transformation**
1. [Table Input Component](https://documentation.matillion.com/docs/1991918)
    - Table Name: `GA_20170721`
    - Column Names: Select all Columns
2. [Filter Component](https://documentation.matillion.com/docs/1991641)

| Input Column | Qualifier | Comparator | Value |
| ------ | ------ | ------ | ------ |
| subcontinent | Is | Like | '%Europe%' |

3. [Calculator Component](https://documentation.matillion.com/docs/1991925) 
    - Calculations: Condense Subcontinents field into two regions:
        - `CASE WHEN ("subcontinent" = 'Western Europe' OR "subcontinent" = 'Northern Europe') THEN 'NW Europe' ELSE CASE WHEN ("subcontinent" = 'Southern Europe' OR "subcontinent" = 'Eastern Europe') THEN 'SE Europe' ELSE "subcontinent" END END`
4. [Convert Type Component](https://documentation.matillion.com/docs/1991926)    

| Column | Type | Size | Precision | Format |
| ------ | ------ | ------ | ------ | ------ |
| date | Date | 10 | 10 | yyyy-mm-dd |

5. [Aggregate](https://documentation.matillion.com/docs/1991880)
    - Groupings: subcontinent
    - Aggregations
    
    | Column | Type |
    | ------ | ------ |
    | browser | Count |
    | timeonsite | Average |

6. [Create View Component](https://documentation.matillion.com/docs/2387025)
    -  View Name: transformed_data

**Orchestration, Part 2**

1. Error Handling
    - Create "Success" and "Failure" paths to respective [SNS Message components](https://documentation.matillion.com/docs/2103738)
    - Create an SNS Topic and subscribe your email to it
    - Configure SNS Component to send a message to that topic

2. [Or Component](https://documentation.matillion.com/docs/2043149) 

3. [Python Script Component](https://documentation.matillion.com/docs/2234735)